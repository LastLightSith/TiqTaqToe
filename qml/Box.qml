import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Layouts 1.3

Rectangle{
	id: root
	property bool atd: true
	property string xo : "empty";
	property alias imgXsrc : x.source;
	property alias imgOsrc : o.source;
	color: "#00ffffff"
	border.color: "#b9ff6b"
	border.width: root.width * 0.01

	Layout.fillHeight: true
	Layout.fillWidth: true


	MouseArea{
		id: mousearea
		anchors.fill:parent
		onClicked:
		{
			if(atd && a)
			{
				if(tern %2)
				{
					x.visible = true;
					root.xo = "x";
					numOfX++;

				}
				else
				{
					o.visible = true;
					root.xo = "o";
					numOfO++;
				}
				tern++;
				root.atd = false;
				if(numOfX >2 || numOfO > 2)
				{
					isWin();
				}
			}
		}
	}

	Image {
		id: x
		source: "../Images/X.png"
		visible: false
		width: root.width
		height: root.height
		anchors.centerIn: root
	}

	Image {
		id: o
		source: "../Images/O.png"
		visible:  false
		width: root.width
		height: root.height
		anchors.centerIn: root
	}
}
