import QtQuick 2.10
import QtQuick.Window 2.10
import com.SmitLinuxUser 1.0
import QtQuick.Layouts 1.3


Window{

	id:root;
	objectName: "root"
	title: "TiqTaqToe"
	color:"#242424"
//	visibility: Window.FullScreen

	property int  tern: 3
	Reset{id:reset}

	property int numOfX: 0;
	property int numOfO: 0;
	property bool a: true
	width: 700
	height: 640

	GridLayout{

		id: grid
		columnSpacing: 0
		rowSpacing: 0
		columns:3;
		anchors.fill: parent


		Box {
			objectName: "b1" ; id:b1
		}
		Box {
			objectName: "b2" ; id:b2
		}
		Box {
			objectName: "b3"; id:b3
		}
		Box {
			objectName: "b4" ; id:b4
		}
		Box {
			objectName: "b5"; id:b5
		}
		Box {
			objectName: "b6" ; id:b6
		}
		Box {
			objectName: "b7" ; id:b7
		}
		Box {
			objectName: "b8" ; id:b8
		}
		Box {
			objectName: "b9" ; id:b9
		}
	}

	function isWin(){

		var k = null;


		if(b1.xo === b2.xo && b2.xo === b3.xo && b1.xo !== "empty")
			k = [b1,b2,b3]
		else if(b4.xo === b5.xo && b5.xo === b6.xo && b6.xo !== "empty")
			k= [b4,b5,b6]
		else if(b7.xo === b8.xo && b8.xo === b9.xo && b9.xo !== "empty")
			k = [b7,b8,b9]
		else if(b2.xo === b5.xo && b5.xo === b8.xo && b8.xo !== "empty")
			k = [b2,b5,b8]
		else if(b3.xo === b6.xo && b6.xo === b9.xo && b3.xo !== "empty")
			k = [b3,b6,b9]
		else if(b1.xo === b4.xo && b4.xo === b7.xo && b4.xo !== "empty")
			k = [b1,b4,b7]
		else if(b1.xo === b5.xo && b5.xo === b9.xo && b5.xo !== "empty")
			k = [b1,b5,b9]
		else if(b7.xo === b5.xo && b5.xo === b3.xo && b3.xo !== "empty")
			k = [b7,b5,b3]
		else if( [b1.xo,b2.xo,b3.xo,b4.xo,b5.xo,b6.xo,b7.xo,b8.xo,b9.xo].indexOf("empty") == -1 ){

			root.a = false;
			reset.resetIt("Draw");
		}


		if(k!==null )
		{
			var str = k[0];
			if(k[0].xo === "x")
			{
				k[0].imgXsrc = "../Images/XR.png"
				k[1].imgXsrc = "../Images/XR.png"
				k[2].imgXsrc = "../Images/XR.png"
				str = "X has Won";
			}
			else
			{
				k[0].imgOsrc = "../Images/OR.png"
				k[1].imgOsrc = "../Images/OR.png"
				k[2].imgOsrc = "../Images/OR.png"
				str = "O has Won";
			}

			root.a = false;
			reset.resetIt(str);
		}
	}
}

