#ifndef RESET_H
#define RESET_H

#include <QObject>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

class Reset : public QObject
{
		Q_OBJECT

private slots:
		void restart();

public:
		explicit Reset(QObject *parent = nullptr);

		Q_INVOKABLE void resetIt(QString str = "Some one has won");

private:
		QGridLayout *grid;
		QPushButton *bexit,*breset;
		QWidget *win;
		QLabel *label;
};

#endif // RESET_H
