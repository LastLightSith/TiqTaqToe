#include <QApplication>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQuickWindow>
#include "reset.h"

Q_DECL_EXPORT int main(int argc,char **argv)
{
		QApplication app(argc,argv);
		QQmlEngine eng;
		qmlRegisterType<Reset>("com.SmitLinuxUser",1,0,"Reset");

		QQmlComponent component(&eng,QUrl("qrc:/qml/main.qml"));
		QObject *obj = component.create();
		QQuickWindow *win = qobject_cast<QQuickWindow *>(obj);
		win->show();

		return  app.exec();
}
