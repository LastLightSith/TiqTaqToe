#include "reset.h"
#include <QDebug>
#include <unistd.h>


Reset::Reset(QObject *parent):
		QObject (parent)
{

		win = new QWidget;

		grid  = new QGridLayout(win);

		bexit = new QPushButton();
		breset = new QPushButton;

		bexit->setText("Exit");
		breset->setText("Play Again");

		label = new QLabel();
		label->setStyleSheet("font-weight:700;font-size:20;qproperty-alignment:AlignCenter;");

		grid->addWidget(label,0,0,1,2);
		grid->addWidget(bexit,1,0);
		grid->addWidget(breset,1,1);
		grid->setSpacing(12);

		win->setWindowTitle("GameOver");

		connect(bexit,SIGNAL(clicked()),this,SLOT(restart()));
		connect(breset,SIGNAL(clicked()),this,SLOT(restart()));

}

void Reset::resetIt(QString str)
{

		label->setText(str);
		qDebug() << "Resetting or quting depending on user\n";

		win->show();
}

void Reset::restart()
{
		QPushButton *s = qobject_cast<QPushButton*>(sender());
		QString str = s->text();
		if(str == bexit->text())
				exit(0);
		else if(str == breset->text() )
		{
				qDebug() << "Restarting\n";
				execl("./TiqTacToeAn",nullptr,nullptr);
				exit(1);
				qDebug() << "Restarting\n";
		}
		else
				qDebug() << "Some error has occured";
}
